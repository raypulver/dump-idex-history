'use strict';

const request = require('bluebird').promisify(require('request'));
const url = require('url');
const { property } = require('lodash');
request({
  method: 'GET',
  url: url.format({
    pathname: '/public',
    hostname: 'poloniex.com',
    protocol: 'https:'
  }),
  qs: {
    command: 'returnTradeHistory',
    currencyPair: 'USDT_ETH',
    start: Math.floor(Date.now()/1000) - (60*60*24),
    end: Math.floor(Date.now()/1000)
  }
}).then((v) => console.log(v));
//  .then(JSON.parse)
//  .then((v) => console.log(v));
