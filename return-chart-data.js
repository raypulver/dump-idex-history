'use strict';

const request = require('bluebird').promisify(require('request'));
const url = require('url');
const { property } = require('lodash');

const fetchTimeSeries = (start, end) => request({
  method: 'GET',
  url: url.format({
    hostname: 'poloniex.com',
    pathname: '/public',
    protocol: 'https:'
  }),
  qs: {
    command: 'returnChartData',
    currencyPair: 'USDT_ETH',
    start,
    period: 14400,
    end
  }
}).then(property('body'))
  .then(JSON.parse)
  .then((ary) => ary.map((v) => ({
    date: v.date,
    price: v.weightedAverage
  })));

module.exports = fetchTimeSeries;
