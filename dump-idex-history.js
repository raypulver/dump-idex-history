#!/usr/bin/env node
'use strict';

const request = require('bluebird').promisify(require('request'));
const { argv } = require('yargs');
const { property } = require('lodash');
const { api } = require('./config');
const { EOL } = require('os');
const { writeFile } = require('fs-promise');
const out = argv.o || 'out.csv';
const moment = require('moment');
const fetchTimeSeries = require('./return-chart-data');
const getCompleteHistory = () => (() => {
  let start = 0, end = Date.now(), all = [];
  return (function get() {
    return request({
      method: 'POST',
      json: {
        start,
        end
      },
      url: api + '/returnTradeHistoryMeta'
    }).then(property('body')).then((body) => {
      if (body.error) throw Error(body.error);
      return body;
    }).then(({ meta, trades }) => {
      all = all.concat(Object.keys(trades).reduce((r, v) => r.concat(trades[v].map((trade) => {
        trade.base = v.split('_')[0];
        trade.trade = v.split('_')[1];
        return trade;
      })), []).sort((a, b) => b.timestamp - a.timestamp));
      if (meta.nextTime) {
        start = meta.nextTime;
        return get();
      }
      return all;
    });
  })();
})();

let seriesIdx = 0;

const fetchTimeSeriesReverse = (start, end) => fetchTimeSeries(start, end).then((data) => data.reverse());

Promise.all([
  getCompleteHistory(),
  fetchTimeSeriesReverse(1506398400, 9999999999)
]).then(([
  trades,
  prices
]) => writeFile(out, [
  [
    'Timestamp',
    'Base',
    'Trade',
    'Type',
    'Price',
    'Amount',
    'Total',
    'Buyer Fee',
    'Seller Fee',
    'Maker',
    'Taker',
    'Order Hash',
    'Transaction Hash',
    'Ether Price'
  ].join(',')
].concat(trades.map(({
  timestamp,
  base,
  trade,
  type,
  price,
  amount,
  total,
  buyerFee,
  sellerFee,
  maker,
  taker,
  orderHash,
  transactionHash
}) => [
  moment(new Date(+timestamp * 1000)).format(',
  base,
  trade,
  type,
  price,
  amount,
  total,
  buyerFee,
  sellerFee,
  maker,
  taker,
  orderHash,
  transactionHash,
  (() => {
    let idx = prices.slice(seriesIdx).findIndex((v) => v.date < timestamp);
    if (!~idx) idx = seriesIdx;
    else idx = Math.max(seriesIdx + idx - 1, 0);
    seriesIdx = idx;
    console.log(seriesIdx);
    return prices[seriesIdx].price;
  })()
].join(','))).join(EOL)))
  .then(() => console.log('output saved to ' + out))
  .catch((err) => console.log(err.stack));
